import sys
from cx_Freeze import setup, Executable

setup(
    name = "FleetGPS python nodejs",
    version = "2.1",
    description = "Service to run fleet python and nodejs.",
    executables = [Executable("multiple.py", base = "Win32GUI")])